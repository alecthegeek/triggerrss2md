# Trigger a GitLab Multi Project CI Pipeline

This repo does nothing but trigger a pipeline in a different project.

See https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#use-a-child-pipeline-configuration-file-in-a-different-project

## Why?

It's part of a demo that shows how to update a project readme with the contents of an RSS feed

See https://gitlab.com/alecthegeek/rss2markdown/ (the downstream project)

So add this API call to your Blog repo and you can update your GitLab profile page with new blog contents (for instance)